package com.example.student.performanceassessmentper5;


import android.support.v4.app.Fragment;


public class PerformanceActivity extends SingleFragmentActivity {


    @Override
    protected Fragment createdFragment() {
        return new TodoListFragment();
    }

    @Override
    protected int getLayoutResId() {

        return 0;
    }


}
